module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    criticalcss: {
            custom_options: {
                options: {
                    url: "file:///Users/lennard/git/~Tests/AboveFold/index.html",
                    width: 400,
                    height: 959,
                    outputfile: "dist/critical.css",
                    filename: "css/style.css",
                    buffer: 800*1024
                }
            }
        },
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-criticalcss');

  // Default task(s).
  grunt.registerTask('default', ['criticalcss']);

};
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    uglify: {
      build: {
        src: ['js/libs/jquery-1.11.1.js', 'js/libs/bootstrap.js', 'js/main.js'],
        dest: 'js/build/main.min.js'
      }
    },

    sass: {
      dist: {
        options: {
          style: 'compressed'
        },
        files: {
          'css/style.css': 'sass/style.scss'
        }
      }
    },

    watch: {
      sass: {
        // We watch and compile sass files as normal but don't live reload here
        files: ['sass/partials/*.scss'],
        tasks: ['sass'],
      },
      uglify: {
        // We watch and compile sass files as normal but don't live reload here
        files: ['js/main.js'],
        tasks: ['uglify'],
      },
    }

  });

  // Show Watch info
  grunt.event.on('watch', function(action, filepath, target) {
    grunt.log.writeln(target + ': ' + filepath + ' has ' + action);
  });

  // Load the plugin that provides the tasks.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', ['watch']);

};